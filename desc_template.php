<?php
function fieldBuilder($array) {
	$fieldString = "";
	foreach($array as $a) {
		$display_a = strtoupper($a);
		$fieldString .= <<<END_BLOCK
		<p>
			<label for="{$a}">{$display_a}</label>
			<input type="text" id="{$a}" name="{$a}">
		</p>
END_BLOCK;
	}
	$fieldString .= "<br>";
	return $fieldString;
}

// radio options box builder for exclusive options
// options box is styled as table-display
// array must have the following structure:
/*
$radios[] = array(
	'name' => "value",
	'vals' => array('key'=>"value", 'key'=>""),
	'checked' => "member of 'vals'"
	);
*/
function buildRadios($array) {
	$radioString = <<<END_BLOCK
		<div id="table">
		<div class="tableRow">
END_BLOCK;

	foreach($array as $contents) {
		$name = ucwords($contents['name']);
		$radioString .= <<<END_BLOCK
			<p class="tableCell">
			<fieldset id="{$contents['name']}">
			  <legend>{$name}</legend>
END_BLOCK;

		foreach($contents['vals'] as $label=>$submit) {
			$checked = ($submit == $contents['checked']) ? " checked" : "";
			$radioString .= "<input type=\"radio\" name=\"".$contents['name']."\""
				." value=\"{$submit}\"{$checked}> {$label}<br>";
		}

		$radioString .= <<<END_BLOCK
			</fieldset>
			</p>
END_BLOCK;
	}

	$radioString .= <<<END_BLOCK
		</div>
		</div>
END_BLOCK;

	return $radioString;
} // buildRadios()

// checkbox options box builder for non-exclusive options
// kind of like radio menus above, but no boxes are prechecked
// array must have the following structure:
/*
$checks[] = array(
	'name' => "value",
	'vals' => array('Tag content'=>"content_for_value_attribute", etc. ...),
	'checked' => "member of 'vals'"
	);
*/
function buildChecks($array) {
	$checkString = <<<END_BLOCK
		<div id="table">
		<div class="tableRow">
END_BLOCK;
	
	// creates fieldset with 'name' attribute set to 'name' from checks array
	foreach($array as $contents) {
	  $name = ucwords($contents['name']);
		$checkString .= <<<END_BLOCK
			<p class="tableCell">
			<fieldset id="{$contents['name']}"">
			  <legend>{$name}</legend>
END_BLOCK;
		
		// inner loop uses 'vals' submembers to build checkboxes
		foreach($contents['vals'] as $label=>$submit) {
			$checkString .= "<input type=\"checkbox\" name=\"{$contents['name']}[]\""
				." value=\"".ucfirst($submit)."\"> {$label}<br>";
		}

		$checkString .= <<<END_BLOCK
			</fieldset>
			</p>
END_BLOCK;
	}

	$checkString .= <<<END_BLOCK
		</div>
		</div>
END_BLOCK;

	return $checkString;
} // buildChecks()

function buildHtml() {
	global $display_block;
	global $pageTitle;
	
	$links[] = array('name'=>"Generic", 'url'=>"./");
	$links[] = array('name'=>"CPU", 'url'=>"cpu.php");
	$links[] = array('name'=>"Motherboard", 'url'=>"mobo.php");
	$links[] = array('name'=>"Receiver", 'url'=>"receiver.php");
	$links[] = array('name'=>"Condition Description Only", 'url'=>"build_condition_description.php");

	$display_links = "<h2>Other templates</h2>";
	foreach($links as $l) {
		$display_links .= "<a href=\"{$l['url']}\">{$l['name']}</a><br>";
	}
	$display_links .= "<hr>";

	$html = <<<END_BLOCK
	<!DOCTYPE html>
	<html>
		<head>
			<title>{$pageTitle}</title>
			<meta charset="utf-8">
			<link type="text/css" rel="stylesheet" href="desc.css">
			<script type="text/javascript">
				window.onload = function(){
					var text_input = document.getElementById ('title');
					text_input.focus();
					text_input.select();
				}
			</script>
		</head>
		<body>
			{$display_links}
			{$display_block}
		</body>
	</html>
END_BLOCK;
	echo $html;
}
?>
