<?php

function save_description_content($description_object, $description_contents) {
	$filename = "../description_content/{$description_object}.html";
	if (!file_exists($filename)) {
		throw new Exception("No such description '{$description_object}'.  To edit a description, please select one from the menu.");
	}

	return file_put_contents($filename, $description_contents);
}

if (isset($_GET['description_object'])) {
	if (isset($_POST['description_contents'])) {
		try {
			save_description_content($_GET['description_object'], $_POST['description_contents']);
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	}
	try {
		$description_object = "../description_content/".$_GET['description_object'].".html";
		$description_contents = file_get_contents($description_object);
	} catch (Exception $e) {
		$description_contents = "";
	}
}

$pages[] = array('description_object' => "the_stuff",
				 'content' => "Generic Template");
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Edit Description</title>
</head>
<body>
	<h2><?php if (isset($error)) echo $error; ?></h2>
	<p>
	<?php
	foreach ($pages as $each_page) {
		echo "<a href='"
		.$_SERVER['PHP_SELF']."?"
		."description_object="
		.$each_page['description_object']
		."'>"
		.$each_page['content']
		."</a><br />"; 
	}
	?>
	</p>
 	<form action="<?php echo $_SERVER['PHP_SELF'].(isset($_GET['description_object']) ? "?description_object=".($_GET['description_object']) : ""); ?>" method="post">
		<input type="hidden" name="description_object" id="description_object" value="<?php if (isset($_GET['description_object'])) { echo $_GET['description_object']; } ?>">
		<textarea name="description_contents" rows="30" cols="100"><?php echo isset($description_contents) ? $description_contents : ""; ?></textarea>
		<p><input type="submit" value="Send"></p>
 	</form>
	<script src="scripts/jquery-2.1.1.min.js"></script>
	<script>
		$(document).ready(function () {
			$("form").submit(function (event) {
				if ($("#description_object").val() === "") {
					alert("No description selected to edit.  Please select a description before submitting.");
					event.preventDefault();
				}
			});
		});
	</script>
</body>
</html>
