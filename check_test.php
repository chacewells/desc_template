<?php
if (!empty($_POST)) {
	echo "<pre>";
	print_r($_POST['damage']);
	echo "</pre>";
}

include 'desc_template.php';
?>

<form action="check_test.php" method="post">

	<?php
	$checks[] = array(
		'name' => "damage",
		'vals' => array('Bad Caps'=>"swollen or burst capacitors",
						'Socket'=>"bent socket pins or damaged socket",
						'Heatsink mount'=>"damaged heatsink mount",
						'Battery seat'=>"damaged or missing battery seat",
						'SATA ports'=>"stripped or damaged SATA ports")
		);

	$built = buildChecks($checks);

	echo $built;
	?>

	<input type="submit">
</form>
