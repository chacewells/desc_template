<?php
include 'thestuff.php';
include 'desc_template.php';

if(!$_POST) {
	$ftitle = "";
	$desc = "";
} else {
	$desc = receiver_stuff($_POST['tested']);

	$title = $_POST['title'];
	$ftitle = "<p align=\"center\"><font size=\"5\" face=\"Arial\">{$title}</font></p>";
}

$heading = "<h1>Build Receiver Description</h1>";

$radios[] = array(
	'name' => "tested",
	'vals' => array('Power Only'=>"power", 'Sound'=>"sound", 'Video'=>"video"),
	'checked' => "power"
	);

$radioString = buildRadios($radios);

$form = <<<END_BLOCK
	<form action="{$_SERVER['PHP_SELF']}" method="POST">
		<p>
			<label for="title">Title: </label>
			<input type="text" id="title" name="title">
		</p>
		{$radioString}
		<input type="submit" value="Build"><hr>
	</form>
END_BLOCK;

$display_block = <<<END_BLOCK
	{$heading}
	{$form}
	<div id="wrapper">
		{$ftitle}
		{$desc}
	</div>
END_BLOCK;

$pageTitle = "Build Receiver Description";

buildHtml();
?>
