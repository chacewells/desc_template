<?php
function conjugate($stringArray) {
	$result = "";

	if (count($stringArray) == 1) strtolower($result);

	for ($i = 0; $i < count($stringArray) - 1; $i++) {
		$result .= "{$stringArray[$i]}";
		$result .= ($i < (count($stringArray) - 2)) ? ", " : " and ";
	}
	$result .= $stringArray[$i];

	return strtolower($result);
}

if(!empty($_POST)) {

	$cosmeticWearString = sprintf("%s cosmetic wear.", ucfirst($_POST['cosmeticWear']));

	if (!empty($_POST['damageType']) && !empty($_POST['damageLocation'])) {
		$damageFormat = "Noticable %s on the %s of the item.  Please see photos for specifics.";
		$damageString = sprintf($damageFormat, conjugate($_POST['damageType']), conjugate($_POST['damageLocation']));
	}

	if (!empty($_POST['accessories'])) {
		$accessoriesFormat = "Comes with %s.";
		$accessoriesString = sprintf($accessoriesFormat, conjugate($_POST['accessories']));
	}

	$emailMe = "Feel free to shoot me an email with any questions.";
	$conditionInfo = "{$cosmeticWearString} "
					."{$damageString} "
					."{$accessoriesString} "
					."{$emailMe}";
}

$tuckerRadios[] = array(
	'name' => "cosmeticWear",
	'vals' => array('minimal' => "minimal",
		'minimal to moderate' => "minimal_to_moderate",
		'moderate' => "moderate",
		'moderate to heavy' => "moderate_to_heavy",
		'heavy' => "heavy"
		),
	'checked' => "minimal"
	);

$tuckerChecks[] = array(
	'name' => "damageType",
	'vals' => array('scratches' => "scratches",
		'scuffs' => "scuffs",
		'dents' => "dents"
		),
	'checked' => "scratches"
	);

$tuckerChecks[] = array(
	'name' => "accessories",
	'vals' => array('antenna(s)'		=>	"antenna(s)",
					'remote'			=>	"remote",
					'batteries'			=>	"batteries",
					'everything in pictures'=>"everything in pictures",
					'instructions'		=>	"instructions",
					'original box'		=>	"original box",
					'nothing'			=>	"nothing",
					'power cords'		=>	"power cords",
					'video cords'		=>	"video cords",
					'audio cords'		=>	"audio cords",
					'installation CD'	=>	"installation CD"
		),
	'checked' => "antenna(s)"
	);

$tuckerChecks[] = array(
	'name' => "damageLocation",
	'vals' => array('top'		=>		"top",
					'sides'		=>		"sides",
					'bottom'	=>		"bottom",
					'edges'		=>		"edges",
					'face/screen' =>	"face/screen"
		),
	'checked' => "scratches"
	);

$tuckerString = buildRadios($tuckerRadios);
$tuckerString .= buildChecks($tuckerChecks);
?>