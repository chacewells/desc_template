<?php
include 'thestuff.php';
include 'desc_template.php';

if(empty($_POST)) {
	$ftitle = "";
	$mobo_include = "";
	$desc = "";
} else {
	
	// damage_include builder
	if (!empty($_POST['damage'])) {
		$damage_include = "<p style='margin-bottom:0px'>This motherboard has the following defect(s):</p>"
		. "<ul style='margin:0px'>";

		foreach ($_POST['damage'] as $damage) {
			$damage_include .= "<li>{$damage}</li>";
		}

		$damage_include .= "</ul>";
	} else {
		$damage_include = "";
	}
	
	// get description based on condition
	$desc = ($_POST['condition'] == "used") ? $thestuff : $badstuff;
	
	// next line formats title
	$ftitle = "<p align=\"center\"><font size=\"5\" face=\"Arial\">{$_POST['title']}</font></p>";
	
	// specific information about the motherboard to include
	$mobo_include = <<<END_BLOCK
		<p align="left">
		</p>
		<p>
			{$damage_include}
		</p>
END_BLOCK;
}

$heading = "<h1>Build Item Description</h1>";

$radios[] = array(
    'name' => "condition",
    'vals' => array('Used'=>"used", 'For parts/repair'=>"parts"),
    'checked' => "used"
    );

$radios[] = array(
	'name' => "cosmetic wear",
	'vals' => array('minimal' => "minimal",
		'minimal to moderate' => "minimal_to_moderate",
		'moderate' => "moderate",
		'moderate to heavy' => "moderate_to_heavy",
		'heavy' => "heavy"
		),
	'checked' => "minimal"
	);

$radioString = buildRadios($radios);

$checks[] = array(
	'name' => "damage type",
	'vals' => array('scratches' => "scratches",
		'scuffs' => "scuffs",
		'dents' => "dents"
		),
	'checked' => ""
	);

$checks[] = array(
	'name' => "accessories",
	'vals' => array('antenna(s)'		=>	"antenna(s)",
					'remote'			=>	"remote",
					'batteries'			=>	"batteries",
					'everything in pictures'=>"everything in pictures",
					'instructions'		=>	"instructions",
					'original box'		=>	"original box",
					'nothing'			=>	"nothing",
					'power cords'		=>	"power cords",
					'video cords'		=>	"video cords",
					'audio cords'		=>	"audio cords",
					'installation CD'	=>	"installation CD"
		),
	'checked' => ""
	);

$checks[] = array(
	'name' => "damage location",
	'vals' => array('top'		=>		"top",
					'sides'		=>		"sides",
					'bottom'	=>		"bottom",
					'edges'		=>		"edges",
					'face/screen' =>	"face/screen"
		),
	'checked' => ""
	);

$checkString = buildChecks($checks);

$form = <<<END_BLOCK
	<form action="{$_SERVER[PHP_SELF]}" method="POST">
		<p>
			<label for="title">Title: </label>
			<input type="text" id="title" name="title">
		</p>
		{$fieldString}
		{$radioString}
		{$checkString}
		<input type="submit" value="Build"><hr>
	</form>
END_BLOCK;

$display_block = <<<END_BLOCK
	{$heading}
	{$form}
	<div id="wrapper">
		{$ftitle}
		{$mobo_include}
		{$desc}
	</div>
END_BLOCK;

$pageTitle = "Build Motherboard Description";

buildHtml();
?>
<?php
include 'desc_template.php';


echo $tuckerString;
?>
