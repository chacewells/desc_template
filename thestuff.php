<?php
function the_stuff() { return file_get_contents("description_content/the_stuff.html"); }

function bad_stuff() { return file_get_contents("description_content/bad_stuff.html"); }

function new_stuff() { return file_get_contents("description_content/new_stuff.html"); }

function receiver_stuff ($working_component) {
	$file_name = "description_content/receiver_stuff_{$working_component}.html";
	
	return file_get_contents($file_name);
}
?>
