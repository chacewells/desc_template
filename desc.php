<?php
include 'thestuff.php';
include 'desc_template.php';

function conjugate($stringArray) {
	$result = "";

	if (count($stringArray) == 1) strtolower($result);

	for ($i = 0; $i < count($stringArray) - 1; $i++) {
		$result .= "{$stringArray[$i]}";
		$result .= ($i < (count($stringArray) - 2)) ? ", " : " and ";
	}
	$result .= $stringArray[$i];

	return strtolower($result);
}

if(!$_POST) {
    $ftitle = "";
    $desc = "";
} else {
	$cosmeticWearString = sprintf("%s cosmetic wear.", ucfirst($_POST['cosmeticWear']));

	if (!empty($_POST['damageType']) && !empty($_POST['damageLocation'])) {
		$damageFormat = "Noticable %s on the %s of the item.  Please see photos for specifics.";
		$damageString = sprintf($damageFormat, conjugate($_POST['damageType']), conjugate($_POST['damageLocation']));
	}

	if (!empty($_POST['accessories'])) {
		$accessoriesFormat = "Comes with %s.";
		$accessoriesString = sprintf($accessoriesFormat, conjugate($_POST['accessories']));
	}

	$emailMe = "Feel free to shoot me an email with any questions.";
	$conditionInfo = "{$cosmeticWearString} "
					."{$damageString} "
					."{$accessoriesString} "
					."{$emailMe}";
	
    $condition = $_POST['condition'];

    $desc = $condition == "used" ? $thestuff : $badstuff;
    switch ($condition) {
    	case "used":
    		$desc = $thestuff;
    		break;
   		case "new":
   			$desc = $newstuff;
   			break;
   		case "parts":
   			$desc = $badstuff;
   			break;
   		default:
   			$desc = "";
    }
    
    $title = $_POST['title'];
    $ftitle = "<p align=\"center\"><font size=\"5\" face=\"Arial\">{$title}</font></p>";
}

$radios[] = array(
        'name' => "condition",
        'vals' => array('Used'  =>  "used",
                        'New'   =>  "new",
                        'Parts' =>  "parts"
                        ),
        'checked' => "used"
        );

$radios[] = array(
	'name' => "cosmeticWear",
	'vals' => array('minimal' => "minimal",
		'minimal to moderate' => "minimal_to_moderate",
		'moderate' => "moderate",
		'moderate to heavy' => "moderate_to_heavy",
		'heavy' => "heavy"
		),
	'checked' => "minimal"
	);

$tuckerChecks[] = array(
	'name' => "damageType",
	'vals' => array('scratches' => "scratches",
		'scuffs' => "scuffs",
		'dents' => "dents"
		),
	'checked' => "scratches"
	);

$tuckerChecks[] = array(
	'name' => "damageLocation",
	'vals' => array('top'		=>		"top",
					'sides'		=>		"sides",
					'bottom'	=>		"bottom",
					'edges'		=>		"edges",
					'face/screen' =>	"face/screen"
		),
	'checked' => "scratches"
	);

$tuckerChecks[] = array(
	'name' => "accessories",
	'vals' => array('antenna(s)'		=>	"antenna(s)",
					'remote'			=>	"remote",
					'batteries'			=>	"batteries",
					'everything in pictures'=>"everything in pictures",
					'instructions'		=>	"instructions",
					'original box'		=>	"original box",
					'nothing'			=>	"nothing",
					'power cords'		=>	"power cords",
					'video cords'		=>	"video cords",
					'audio cords'		=>	"audio cords",
					'installation CD'	=>	"installation CD"
		),
	'checked' => "antenna(s)"
	);

$tuckerString = buildRadios($radios);
$tuckerString .= buildChecks($tuckerChecks);

$heading = "<h1>Build Item Description</h1>";
$form = <<<END_BLOCK
    <form action="{$_SERVER[PHP_SELF]}" method="POST">
        <p>
    	    <label for="title">Title: </label>
    	    <input type="text" id="title" name="title">
        </p>
        <div id="table">
            <div class="tableRow">
                <p class="tableCell" style="float:left">
					{$tuckerString}
                </p>
            </div>
            <div class="tableRow">
            </div>
        </div>
        <input type="submit" value="Build">
    </form><hr>
END_BLOCK;

$display_block = <<<END_BLOCK
    {$heading}
    {$form}
    <div id="wrapper">
        {$ftitle}
		{$conditionInfo}
        {$desc}
    </div>
END_BLOCK;

$pageTitle = "Build Item Description";

buildHtml();
?>
