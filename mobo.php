<!-- mobo.php -->
<?php
include 'thestuff.php';
include 'desc_template.php';

if(empty($_POST)) {
	$ftitle = "";
	$mobo_include = "";
	$desc = "";
} else {
	$cpu = $_POST['cpu'];
	$ram = $_POST['ram'];
	$io = $_POST['io'];
	$condition = $_POST['condition'];

	// disclaimer builder
	if($cpu || $ram) {
		$cpu_string = $cpu ? "CPU" : "";
		$ram_string = $ram ? "RAM" : "";
		$slash_string = ($cpu && $ram) ? "/" : "";
		$disclaimer = "If you want the {$cpu_string}{$slash_string}{$ram_string} with the board,"
			." the Buy-It-Now price is firm."
			."  If you don't need the {$cpu_string}{$slash_string}{$ram_string}, feel free to make me an offer!";
	} else {
		$disclaimer = "";
	}
	
	// parts_include builder
	if($cpu || $ram || $io) {
		$cpu_include = $cpu ? " $cpu CPU " : "";
		$amp1 = (($cpu && $ram) || ($cpu && $io)) ? "&amp;" : "";
		$ram_include = $ram ? " $ram RAM " : "";
		$amp2 = ($ram && $io) ? "&amp;" : "";
		$io_include = $io ? " I/O Shield" : "";
		$parts_include = "Board includes {$cpu_include}{$amp1}{$ram_include}{$amp2}{$io_include}.";
	} else {
		$parts_include = "Board only.";
	}
	
	// explicit mention of missing I/O shield
	if(!$io) {
		$parts_include .= " I/O Shield not included.";
	}
	
	// damage_include builder
	if (!empty($_POST['damage'])) {
		$damage_include = "<p style='margin-bottom:0px'>This motherboard has the following defect(s):</p>"
		. "<ul style='margin:0px'>";

		foreach ($_POST['damage'] as $damage) {
			$damage_include .= "<li>{$damage}</li>";
		}

		$damage_include .= "</ul>";
	} else {
		$damage_include = "";
	}
	
	// get description based on condition
	$desc = $condition == "used" ? $thestuff : $badstuff;
	
	// next line formats title
	$ftitle = "<p align=\"center\"><font size=\"5\" face=\"Arial\">{$_POST['title']}</font></p>";
	
	// specific information about the motherboard to include
	$mobo_include = <<<END_BLOCK
		<p align="left">
			{$disclaimer}
		</p>
		<p>
			{$parts_include}
			{$damage_include}
		</p>
END_BLOCK;
}

$heading = "<h1>Build Motherboard Description</h1>";

$fields[] = "cpu";
$fields[] = "ram";

$fieldString = fieldBuilder($fields);

$radios[] = array(
	'name' => "io",
	'vals' => array('I/O Shield'=>"1", 'No I/O Shield'=>""),
	'checked' => ""
	);

$radios[] = array(
	'name' => "condition",
	'vals' => array('Used'=>"used", 'For parts/repair'=>"parts"),
	'checked' => "used"
	);

$radioString = buildRadios($radios);

$checks[] = array(
	'name' => "damage",
	'vals' => array('Bad Caps'=>"bad capacitors",
						'Socket pins'=>"bent socket pins or damaged socket",
						'Heatsink mount'=>"damaged heatsink mount",
						'Battery seat'=>"damaged or missing battery seat",
						'SATA ports'=>"stripped or damaged SATA ports",
						'No Power'=>"does not power on or POST",
						'No POST'=>"Powers on, but does not POST",
						'RAM slots'=>"damaged RAM slots")
	);
	
$checkString = buildChecks($checks);

$form = <<<END_BLOCK
	<form action="{$_SERVER['PHP_SELF']}" method="POST">
		<p>
			<label for="title">Title: </label>
			<input type="text" id="title" name="title">
		</p>
		{$fieldString}
		{$radioString}
		{$checkString}
		<input type="submit" value="Build"><hr>
	</form>
END_BLOCK;

$display_block = <<<END_BLOCK
	{$heading}
	{$form}
	<div id="wrapper">
		{$ftitle}
		{$mobo_include}
		{$desc}
	</div>
END_BLOCK;

$pageTitle = "Build Motherboard Description";

buildHtml();
?>
